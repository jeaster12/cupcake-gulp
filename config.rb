require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/app"
css_dir = "app/css"
sass_dir = "app/styles"
images_dir = "app/img"
javascripts_dir = "app/js"
fonts_dir = "app/fonts"

output_style = :nested

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

line_comments = false
color_output = false

preferred_syntax = :scss
